# Copyright (c) 2019 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

from django.test import TestCase, Client
from dashboard.models import PipelineResult, JobResult, TraceResult, PerDeviceResults
from dashboard.views import ci, image_store
import dashboard.cache as cache

from base64 import b64decode
from PIL import Image, ImageChops
from threading import Thread
from time import sleep
import io
import os
import re
import unittest

SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))

class FakeCI:
    def __init__(self):
        self.pipelines = []
        self.jobs = []
        self.time_to_connect = 0.0

        # Monkey patch ci module used by dashboard.views
        ci.get_pipeline_ids = self.get_pipeline_ids_impl
        ci.get_pipeline = self.get_pipeline_impl
        ci.get_test_job = self.get_test_job_impl

    def add_pipeline(self, pid, status, test_job_ids):
        assert(pid < 256 and pid > 0)
        p = ci.Pipeline()
        p.id = pid
        p.project_path = "fakeci"
        p.status = status
        p.mesa_sha = "0a1b2c3d" + hex(pid)[2:] + "4e5f"
        p.mesa_repo = "https://example.com/mesa/mesa"
        p.test_job_ids = test_job_ids

        self.pipelines.append(p)

    def add_job(self, jid, pid, status, trace_results):
        j = ci.Job()
        j.id = jid
        j.project_path = "fakeci"
        j.pipeline_id = pid
        j.status = status
        j.device = "device-%d" % jid
        j.trace_results = trace_results
        j.timestamp = jid
        self.jobs.append(j)

    def get_pipeline_ids_impl(self, project_path):
        return [p.id for p in self.pipelines if p.project_path == project_path]

    def get_pipeline_impl(self, project_path, pid):
        if self.time_to_connect > 0:
            sleep(self.time_to_connect)
        return next(p for p in self.pipelines if p.id == pid and p.project_path == project_path)

    def get_test_job_impl(self, project_path, jid):
        if self.time_to_connect > 0:
            sleep(self.time_to_connect)
        return next(j for j in self.jobs if j.id == jid and j.project_path == project_path)

    def get_pipeline_ids(self):
        return self.get_pipeline_ids_impl("fakeci")

    def get_pipeline(self, pid):
        return self.get_pipeline_impl("fakeci", pid)

    def get_test_job(self, jid):
        return self.get_test_job_impl("fakeci", jid)

class FakeImageStore:
    def __init__(self):
        # Monkey patch image_store module used by dashboard.views
        image_store.reference_png_url = self.reference_png_url
        image_store.actual_png_url = self.actual_png_url
        image_store.fetch = self.fetch

    def reference_png_url(self, checksum):
        return "file://%s/test_data/%s.png" % (SCRIPT_DIR, checksum)

    def actual_png_url(self, project_path, pipeline_id, job_id, checksum):
        return "file://%s/test_data/%s.png" % (SCRIPT_DIR, checksum)

    def fetch(self, url):
        return open(url.replace("file://", ""), "rb").read()

class DashboardTestCase(TestCase):
    def _assertLandingContainsPipeline(self, response, pipeline):
        job_links = ["/dashboard/job/fakeci/" + str(jid) + "/" for jid in pipeline.test_job_ids]
        for job_link in job_links:
            self.assertContains(response, job_link)
        self.assertContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertContains(response, pipeline.url())

    def _assertLandingNotContainsPipeline(self, response, pipeline):
        job_links = ["/dashboard/job/fakeci/" + str(jid) + "/" for jid in pipeline.test_job_ids]
        for job_link in job_links:
            self.assertNotContains(response, job_link)
        self.assertNotContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertNotContains(response, pipeline.url())

    def _assertDatabaseContainsPipeline(self, pipeline):
        pr = PipelineResult.objects.get(id=pipeline.id)
        self.assertEquals(pr.mesa_sha, pipeline.mesa_sha)
        self.assertEquals(
            pr.status,
            cache._ci_to_result_status_map.get(pipeline.status, "unknown")
        )

    def _assertDatabaseContainsJob(self, job):
        jr = JobResult.objects.get(id=job.id)
        self.assertEquals(jr.project_path, job.project_path)
        self.assertEquals(
            jr.status,
            cache._ci_to_result_status_map.get(job.status, "unknown")
        )
        for name, tr_info in job.trace_results.items():
            tr = TraceResult.objects.get(name=name, job_result_id=job.id)
            self.assertEquals(tr.expected_checksum, tr_info["expected"])
            self.assertEquals(tr.actual_checksum, tr_info["actual"])

    def setUp(self):
        self.fake_ci = FakeCI()
        self.fake_image_store = FakeImageStore()

        self.fake_ci.add_job(13, 1, "success",
                            {"trace1": {"actual": "131", "expected": "131"}, "trace2": {"actual": "132", "expected": "132"}})
        self.fake_ci.add_job(14, 2, "failed",
                            {"trace1": {"actual": "141", "expected": "141"}, "trace2": {"actual": "142", "expected": "abc"}})
        self.fake_ci.add_job(15, 4, "success",
                            {"trace1": {"actual": "151", "expected": "152"}, "trace2": {"actual": None, "expected": None}})
        self.fake_ci.add_job(16, 5, "success", {})
        self.fake_ci.add_job(17, 5, "failed", {})

        self.fake_ci.add_pipeline(1, "success", [13])
        self.fake_ci.add_pipeline(2, "failed", [14])
        self.fake_ci.add_pipeline(3, "failed", [])
        self.fake_ci.add_pipeline(4, "success", [15])
        self.fake_ci.add_pipeline(5, "failed", [16, 17])

        self.client = Client()

    def test_db_is_populated_from_ci(self):
        self.fake_ci.time_to_connect = 0.05

        self.client.get('/dashboard/project/fakeci/')

        for p in self.fake_ci.pipelines:
            self._assertDatabaseContainsPipeline(p)

        for j in self.fake_ci.jobs:
            self._assertDatabaseContainsJob(j)

    def test_landing_contains_only_finished_pipelines(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})

        response = self.client.get('/dashboard/project/fakeci/')

        p = self.fake_ci.get_pipeline(100)
        self._assertLandingNotContainsPipeline(response, p)

    def test_landing_contains_job_status(self):
        shown_jobs = []
        for p in self.fake_ci.pipelines:
            for jid in p.test_job_ids:
                shown_jobs.append(self.fake_ci.get_test_job(jid))

        count_pass = len([j for j in shown_jobs if j.status == "success"])
        count_fail = len([j for j in shown_jobs if j.status == "failed"])

        response = self.client.get('/dashboard/project/fakeci/')

        self.assertContains(response, "pass", count=2 * count_pass)
        self.assertContains(response, "fail", count=2 * count_fail)

    def test_landing_contains_job_device(self):
        response = self.client.get('/dashboard/project/fakeci/')

        for p in self.fake_ci.pipelines:
            for jid in p.test_job_ids:
                self.assertContains(response, "device-%d" % jid, count = 1)

    def test_job_view_contains_trace_names_and_links(self):
        response = self.client.get('/dashboard/job/fakeci/13/')
        job = self.fake_ci.get_test_job(13)

        self.assertContains(response, "trace1")
        self.assertContains(response, job.url())
        self.assertContains(response, "trace2")
        self.assertContains(response, job.url())

    def test_job_view_contains_trace_status(self):
        response = self.client.get('/dashboard/job/fakeci/13/')
        self.assertContains(response, "pass", count=3) # 2 traces + summary
        self.assertNotContains(response, "fail")

        response = self.client.get('/dashboard/job/fakeci/14/')
        self.assertContains(response, "pass", count=1)
        self.assertContains(response, "fail", count=2) # 1 trace + summary

    def test_job_view_contains_job_info(self):
        pipeline = self.fake_ci.get_pipeline(5)
        job16 = self.fake_ci.get_test_job(16)
        response = self.client.get('/dashboard/job/fakeci/16/')

        self.assertContains(response, "device-16")
        self.assertContains(response, job16.url())
        self.assertContains(response, pipeline.mesa_repo + "/commit/" + pipeline.mesa_sha)
        self.assertContains(response, pipeline.url())

    def test_job_view_for_running_job_contains_no_results(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})

        response = self.client.get('/dashboard/job/fakeci/200/')
        self.assertNotContains(response, "pass")
        self.assertNotContains(response, "fail")

    def test_job_view_for_running_pipeline_contains_results(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "failed",
                             {"trace1": {"actual": "131", "expected": "131"},
                              "trace2": {"actual": "132", "expected": "132x"}})

        response = self.client.get('/dashboard/job/fakeci/200/')
        self.assertContains(response, "pass", count=1)
        self.assertContains(response, "fail", count=2) # 1 trace + summary

    @unittest.skip("We don't properly handle requests in multiple threads")
    def test_job_view_parallel_fetch_trace_status_no_duplicates(self):
        self.fake_ci.time_to_connect = 0.01
        self.fake_ci.add_job(200, 201, "success",
                             {"trace1": {"actual": "131", "expected": "131"},
                              "trace2": {"actual": "132", "expected": "132"},
                              "trace3": {"actual": "133", "expected": "133"},
                              "trace4": {"actual": "134", "expected": "134"},
                              "trace5": {"actual": "135", "expected": "135"},
                              "trace6": {"actual": "136", "expected": "136"}})

        def fetcher(client):
            response = client.get('/dashboard/job/fakeci/200/')

        threads = []
        for i in range(10):
            threads.append(Thread(target=fetcher, args=(self.client,)))
            threads[-1].start()

        for t in threads:
            t.join()

        response = self.client.get('/dashboard/job/fakeci/200/')
        self.assertContains(response, "pass", count=7) # 6 traces + summary

        self.fake_ci.time_to_connect = 0.0

    def test_database_is_updated_with_final_ci_results(self):
        self.fake_ci.add_pipeline(100, "running", [200])
        self.fake_ci.add_job(200, 100, "running", {})
        p = self.fake_ci.get_pipeline(100)

        self.client.get('/dashboard/project/fakeci/')
        self._assertDatabaseContainsPipeline(p)

        p.status = "success"
        self.fake_ci.get_test_job(200).status = "success"

        # Database should include updated pipeline info
        self.client.get('/dashboard/project/fakeci/')
        self._assertDatabaseContainsPipeline(p)

    def test_namespaced_project_path_is_recognized(self):
        self.fake_ci.add_pipeline(100, "success", [200])
        self.fake_ci.add_job(200, 100, "success", {})
        p = self.fake_ci.get_pipeline(100)
        j = self.fake_ci.get_test_job(200)
        p.project_path = "namespace/project";
        j.project_path = "namespace/project";

        self.assertIn("namespace/project", p.url())
        self.assertIn("namespace/project", j.url())

        self.client.get('/dashboard/job/namespace/project/200/')
        self._assertDatabaseContainsPipeline(p)

    def test_db_is_populated_with_per_device_results(self):
        self.client.get('/dashboard/devices/fakeci/')
        for j in self.fake_ci.jobs:
            pdr = PerDeviceResults.objects.filter(id="fakeci:%s" % j.device)[0]
            self.assertEqual(pdr.name, j.device)
            self.assertEqual(pdr.last_run.id, j.id)
            if j.status == "success":
                self.assertEqual(pdr.last_good_run.id, j.id)
                self.assertEqual(pdr.first_bad_run, None)
            else:
                self.assertEqual(pdr.last_good_run, None)
                self.assertEqual(pdr.first_bad_run.id, j.id)

    def test_devices_view_contains_info(self):
        response = self.client.get('/dashboard/devices/fakeci/')

        # Each device causes the sha to be displayed in 2 columns (either
        # current+failing, or current+pass) and the 2 respective urls
        # (each job corresponds to a different device in this setup).
        for p in self.fake_ci.pipelines:
            self.assertContains(response, p.mesa_sha[:10] + "\n", count=2 * len(p.test_job_ids))
            self.assertContains(response, p.mesa_sha, count=2 * len(p.test_job_ids))

        count_pass = len([j for j in self.fake_ci.jobs if j.status == "success"])
        count_fail = len([j for j in self.fake_ci.jobs if j.status == "failed"])

        self.assertContains(response, "all good", count=count_pass)
        self.assertContains(response, "no good runs yet", count=count_fail)

    def test_devices_view_is_populated_per_project(self):
        self.fake_ci.add_pipeline(100, "success", [200])
        self.fake_ci.add_job(200, 100, "success", {})
        p = self.fake_ci.get_pipeline(100)
        j = self.fake_ci.get_test_job(200)
        p.project_path = "namespace/project";
        j.project_path = "namespace/project";

        response = self.client.get('/dashboard/devices/fakeci/')

        for j in (j for j in self.fake_ci.jobs if j.project_path == "fakeci"):
            self.assertContains(response, j.device)

        for j in (j for j in self.fake_ci.jobs if j.project_path != "fakeci"):
            self.assertNotContains(response, j.device)

    def _assertImageDataMatches(self, actual_png_data, expected_png_filename):
        expected_png_file = open('%s/test_data/%s' % (SCRIPT_DIR, expected_png_filename), 'rb')
        actual_png_file = io.BytesIO(actual_png_data)
        self.assertIsNone(
            ImageChops.difference(
                Image.open(expected_png_file),
                Image.open(actual_png_file)
            ).getbbox()
        )

    def test_image_diff_mismatch(self):
        self.fake_ci.add_pipeline(100, "failed", [200])
        self.fake_ci.add_job(200, 100, "success",
                             {"trace1": {"actual": "actual", "expected": "expected"}})

        response = self.client.get('/dashboard/imagediff/fakeci/200/trace1/')

        png_img_src = re.findall(b'<img src="(file://[^"]+)">', response.content)
        png_b64 = re.findall(b'<img src="data:image/png;base64,([^"]+)">', response.content)

        self.assertEqual(len(png_img_src), 2)
        self.assertEqual(png_img_src[0].decode('utf-8'),
                         self.fake_image_store.reference_png_url("expected"))
        self.assertEqual(png_img_src[1].decode('utf-8'),
                         self.fake_image_store.actual_png_url("fakeci", 100, 200, "actual"))
        self.assertEqual(len(png_b64), 1)
        self._assertImageDataMatches(b64decode(png_b64[0]), "diff.png")

    def test_image_diff_match(self):
        self.fake_ci.add_pipeline(100, "failed", [200])
        self.fake_ci.add_job(200, 100, "success",
                             {"trace1": {"actual": "expected", "expected": "expected"}})

        response = self.client.get('/dashboard/imagediff/fakeci/200/trace1/')

        png_img_src = re.findall(b'<img src="(file://[^"]+)">', response.content)
        png_b64 = re.findall(b'<img src="data:image/png;base64,([^"]+)">', response.content)

        self.assertEqual(len(png_img_src), 1)
        self.assertEqual(png_img_src[0].decode('utf-8'),
                         self.fake_image_store.reference_png_url("expected"))
        self.assertEqual(len(png_b64), 0)
