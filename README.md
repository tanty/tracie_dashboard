Tracie dashboard
================

A web dashboard for the results of the tracie trace-based Mesa CI.

The dashboard is built using Django.

Get dashboard
-------------

    $ git clone https://gitlab.freedesktop.org/gfx-ci/tracie/tracie_dashboard

Development setup
-----------------

### Activate and prepare virtualenv

    $ python3 -m venv tracie-dashboard-venv
    $ source tracie-dashboard-venv/bin/activate
    $ pip install -r tracie_dashboard/requirements.txt

Instructions below assume that the virtualenv is active.

### Prepare the site-specific settings

Copy `tracie_dashboard/local_settings.py.default` to `tracie_dashboard/local_settings.py`
and modify as necessary. This file is loaded at the end of Django's
settings.py when the dashboard is launched, allowing you to override
defaults depending on the environment.

In addition to `local_settings.py`, settings are also customizable
from the parent shell environment, so you can additionally set
`GITLAB_URL`, say, by prefixing the variable with `DJANGO_` in the
parent environment,

    export DJANGO_GITLAB_URL='https://gitlab.example.com'

This will always override anything set in `local_settings.py`.

This level of customization is often useful in automated pipelines, such as
in CI and is required by the Docker integration (see below). An example
shell environment ready to be sourced can be found in
`tracie_dashboard/setup-env.sh.sample`

For local development, editing the `tracie_dashboard/local_settings.py`
is recommended. This file is in `.gitignore` and should not be committed.

A gitlab access token can be generated from your gitlab.freedesktop.org
profile, in the "Personal Access Token" section. The gitlab access token must
be created with the "api" scope.

Set `DEBUG` to `True` so static files are served by Django and for numerous
other local development niceties.

Now change into `tracie_dashboard/` and create the initial database tables:

    tracie_dashboard$ python3 manage.py migrate

### Run dashboard

First ensure you have configured your environment by creating the `local_settings.py`
as explained above.

For a development run of the dashboard (note that first run may take some time
while data is downloaded from GitLab and cached):

    tracie_dashboard$ python3 manage.py runserver

Run the tests with:

    tracie_dashboard$ python3 manage.py test

If you need to clear the objects cached from GitLab and start from fresh:

    tracie_dashboard$ python3 manage.py clearcache

Using Docker
------------

A `Dockerfile` is provided for creating an instance of the dashboard powered by
uwsgi and nginx. To create the docker image run:

    tracie_dashboard$ docker build --tag tracie_dashboard .

To run the latest built image a convenience script is provided:

    tracie_dashboard$ ./start-docker.sh [--host-port=PORT] [--docker-image=IMAGE]

The default values for PORT and IMAGE are '80' and 'tracie_dashboard:latest',
respectively.

Note that `start-docker.sh` depends on a properly populated `setup-env.sh` which
can be copied from `tracie_dashboard/setup-env.sh.sample` and modified as
required.
